# [Fish](https://fishshell.com/)

💜 Fish is one of the saniest software I've seen.

Almost vanilla [config](/users/shared/fish/config.fish), a lot of faster than zsh/zim bundle with the same functionality.

Minimalistic [prompt](/users/shared/fish/functions/fish_prompt.fish) with git integration:

![](/assets/screens/fish.png)

Use `fish_config` to configure it with GUI.

## Change directory by partial match with [zoxide](https://github.com/ajeetdsouza/zoxide#getting-started)

`j nix` will switch to `/etc/nixos`\
`j do` will switch to `/home/ksevelyar/downloads`

## Fuzzy change directory with FZF

Press `Alt+C`

## Fuzzy find file

Type `v <Alt>-t` or `v -o (fzf)`
